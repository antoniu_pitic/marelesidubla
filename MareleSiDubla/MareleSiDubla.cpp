#include "MareleSiDubla.h"
#include "GeneratorZaruri.h"

#include <iostream>
using namespace std;

void Joc(Zar Z)
{

	int z1 = Z.GetNewDice();
	int z2 = Z.GetNewDice();
	int z3 = Z.GetNewDice();
	int z4 = Z.GetNewDice();

	AfisareZaruri(z1, z2);
	AfisareZaruri(z3, z4);

	int rezultat = MareleSiDubla(z1, z2, z3, z4);
	AfisareCastigator(rezultat);
}

int Marele(int z1, int z2, int z3, int z4)
{
	int s1 = z1 + z2;
	int s2 = z3 + z4;

	if (s1 > s2) {
		return 1;
	}
	if (s1 < s2) {
		return 2;
	}
	if (s1 == s2) {
		return 0;
	}
}

int MareleSiDubla(int z1, int z2, int z3, int z4)
{
	bool dubla1 = (z1 == z2);
	bool dubla2 = (z3 == z4);

	if (dubla1 && !dubla2) {
		return 1;
	}
	else {
		if (!dubla1 && dubla2) {
			return 2;
		}
		else{
			return Marele(z1, z2, z3, z4);
		}
	}

}

void AfisareZaruri(int zar1, int zar2)
{
	cout << "(" << zar1 << ",";
	cout << zar2 << ")   ";
}

void AfisareCastigator(int castigator){
	if (castigator == 1) {
		cout << "Primu" << endl;
	}
	if (castigator == 2) {
		cout << "Al doilea"<< endl;
	}
	if (castigator == 0) {
		cout << "Egal"<< endl;
	}
}
