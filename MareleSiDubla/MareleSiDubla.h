#pragma once
#include "GeneratorZaruri.h"

void Joc(Zar Z);

int Marele(int z1, int z2, int z3, int z4);
int MareleSiDubla(int z1, int z2, int z3, int z4);

void AfisareZaruri(int zar1, int zar2);

void AfisareCastigator(int castigator);